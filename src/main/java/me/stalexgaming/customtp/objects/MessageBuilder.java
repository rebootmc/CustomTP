package me.stalexgaming.customtp.objects;

import me.stalexgaming.customtp.utils.Color;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alex on 5-7-2016.
 */
public class MessageBuilder {

    private List<String> message;
    private Map<String, String> replacers;

    private Request request = null;

    private boolean special = false;

    public MessageBuilder(List<String> message) {
        this.message = message;
        this.replacers = new HashMap<>();
    }

    public MessageBuilder replace(String from, String to) {
        replacers.put(from, to);
        return this;
    }

    public MessageBuilder setSpecial(Request request) {
        this.special = true;
        this.request = request;
        return this;
    }

    public void send(Player p) {
        if(!special) {
            message.forEach(line -> {
                String s = line;
                for(Map.Entry<String, String> entry : replacers.entrySet()) {
                    s = s.replace(entry.getKey(), entry.getValue());
                }
                p.sendMessage(Color.np(s));
            });
        } else {
            message.forEach(line -> {
                String s = line;
                for(Map.Entry<String, String> entry : replacers.entrySet()) {
                    s = s.replace(entry.getKey(), entry.getValue());
                }
                if((line.contains("[deny]") && line.contains("[-deny]")) || (line.contains("[accept]") && line.contains("[-accept]"))) {
                    List<TextComponent> components = new ArrayList<>();
                    String part = "";
                    for(String word : s.split(" ")) {
                        if(word.startsWith("[deny]") || word.startsWith("[accept]")) {
                            if(!part.isEmpty()) {
                                components.add(new TextComponent(part));
                            }
                            part = word;
                        } else if(word.endsWith("[-accept]") || word.endsWith("[-deny]")) {
                            String w = word.endsWith("[-accept]") ? "[-accept]" : "[-deny]";
                            if(part.startsWith(w.replace("-", ""))) {
                                TextComponent component = new TextComponent(Color.np(" " + part.replace(w.replace("-", ""), "").replace(w, "")));
                                component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, word.contains("deny") ? "/tpdeny" : "/tpaccept"));
                                components.add(component);
                                part = "";
                            } else {
                                part += Color.np(" " + word);
                            }
                        } else {
                            part += Color.np(" " + word);
                        }
                    }
                    if(!part.isEmpty()) {
                        components.add(new TextComponent(part));
                    }
                    TextComponent finalComponent = new TextComponent("");
                    for(TextComponent component : components) {
                        if(!component.toPlainText().equalsIgnoreCase("")) {
                            finalComponent.addExtra(component);
                        }
                    }

                    p.spigot().sendMessage(finalComponent);
                } else {
                    p.sendMessage(Color.np(s));
                }
            });
        }
    }

}
