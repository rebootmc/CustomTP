package me.stalexgaming.customtp.objects;

import me.stalexgaming.customtp.Main;
import me.stalexgaming.customtp.enums.TeleportationType;
import me.stalexgaming.customtp.managers.RequestManager;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by alex on 5-7-2016.
 */
public class Request {

    public static List<UUID> tpingPlayers = new ArrayList<>();

    private static final RequestManager requestManager = Main.getRequestManager();

    private static final long EXPIRE_TIME = Main.getInstance().getConfig().getLong("tpa_expire_time")*1000;
    private static final long TP_TIME = Main.getInstance().getConfig().getLong("tpa_time")*20;

    private Player from;
    private Player to;
    private long timeStamp;
    private TeleportationType teleportationType;

    public Request(Player from, Player to, long timeStamp, TeleportationType teleportationType) {
        this.from = from;
        this.to = to;
        this.timeStamp = timeStamp;
        this.teleportationType = teleportationType;
    }

    public void deny() {
        new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpa_denied_denier")).replace("%player%", from.getName()).send(to);
        new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpa_denied_sender")).replace("%player%", to.getName()).send(from);
        requestManager.deleteRequest(this);
    }

    public void accept() {
        Player teleporting;
        Player to;
        if(teleportationType == TeleportationType.TPA) {
            teleporting = this.from;
            to = this.to;
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpa_accepted_receiver")).replace("%player%", teleporting.getName()).send(to);
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpa_accepted_teleporter")).replace("%player%", to.getName()).send(teleporting);
        } else {
            teleporting = this.to;
            to = this.from;
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpahere_accepted_receiver")).replace("%player%", teleporting.getName()).send(to);
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpahere_accepted_teleporter")).replace("%player%", to.getName()).send(teleporting);
        }
        tpingPlayers.add(teleporting.getUniqueId());
        new BukkitRunnable() {
            int i = 0;
            @Override
            public void run() {
                if(!tpingPlayers.contains(teleporting.getUniqueId())) {
                    this.cancel();
                    new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpa_cancelled_movement_mover")).replace("%player%", to.getName()).send(teleporting);
                    new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpa_cancelled_movement_receiver")).replace("%player%", teleporting.getName()).send(to);
                    // Teleportation cancelled.
                    requestManager.deleteRequest(Request.this);
                }
                if(i >= TP_TIME) {
                    this.cancel();
                    teleporting.teleport(to);
                    new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpa_teleported_teleporter")).replace("%player%", to.getName()).send(teleporting);
                    new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpa_teleported_receiver")).replace("%player%", teleporting.getName()).send(to);
                    requestManager.deleteRequest(Request.this);
                }
                i++;
            }
        }.runTaskTimer(Main.getInstance(), 0, 1);
    }

    public boolean isValid() {
        return !(System.currentTimeMillis() > timeStamp+EXPIRE_TIME);
    }

    public Player getFrom() {
        return from;
    }

    public Player getTo() {
        return to;
    }

    public long getTimeStamp() {
        return timeStamp;
    }
}
