package me.stalexgaming.customtp.listeners;

import me.stalexgaming.customtp.objects.Request;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by alex on 5-7-2016.
 */
public class PlayerMove implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if((e.getFrom().getBlockX() != e.getTo().getBlockX()) || e.getFrom().getBlockZ() != e.getTo().getBlockZ()) {
            if(Request.tpingPlayers.contains(e.getPlayer().getUniqueId())) {
                Request.tpingPlayers.remove(e.getPlayer().getUniqueId());
            }
        }
    }

}
