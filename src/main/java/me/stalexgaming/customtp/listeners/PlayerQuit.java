package me.stalexgaming.customtp.listeners;

import me.stalexgaming.customtp.managers.RequestManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by alex on 5-7-2016.
 */
public class PlayerQuit implements Listener {

    private RequestManager requestManager;

    public PlayerQuit(RequestManager requestManager) {
        this.requestManager = requestManager;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        requestManager.removeRequests(e.getPlayer());
    }

}
