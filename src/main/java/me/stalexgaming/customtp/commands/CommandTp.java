package me.stalexgaming.customtp.commands;

import me.stalexgaming.customtp.Main;
import me.stalexgaming.customtp.objects.MessageBuilder;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by alex on 5-7-2016.
 */
public class CommandTp extends AbstractCommand {

    public CommandTp(String name, int minArgs, int maxArgs, boolean console, String usage) {
        super(name, minArgs, maxArgs, console, usage);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        Player p = (Player) sender;
        if(!p.hasPermission("customtp.tp")) {
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.no_perm")).send(p);
            return true;
        }
        
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(player.getDisplayName().startsWith(args[0].replaceFirst("\\*", ""))) {
                p.teleport(player);
                new MessageBuilder(Main.getInstance().getConfig().getStringList("message.force_teleported")).replace("%player%", player.getName()).replace("%nickname%", player.getDisplayName()).send(p);
                return true;
            }
        }
        
        if(Bukkit.getPlayer(args[0]) != null) {
            p.teleport(Bukkit.getPlayer(args[0]));
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.force_teleported")).replace("%player%", Bukkit.getPlayer(args[0]).getName()).replace("%nickname%", player.getDisplayName()).send(p);
            return true;
        }
        
        new MessageBuilder(Main.getInstance().getConfig().getStringList("message.invalid_player")).replace("%args%", args[0]).send(p);
        return true;
    }
}
