package me.stalexgaming.customtp.commands;

import me.stalexgaming.customtp.Main;
import me.stalexgaming.customtp.managers.RequestManager;
import me.stalexgaming.customtp.objects.MessageBuilder;
import me.stalexgaming.customtp.objects.Request;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by alex on 5-7-2016.
 */
public class CommandTpDeny extends AbstractCommand {

    private RequestManager requestManager;

    public CommandTpDeny(String name, int minArgs, int maxArgs, boolean console, String usage, RequestManager requestManager) {
        super(name, minArgs, maxArgs, console, usage);
        this.requestManager = requestManager;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        Player p = (Player) sender;
        if(!requestManager.hasIncomingRequest(p) || Request.tpingPlayers.contains(p.getUniqueId()) || (requestManager.hasIncomingRequest(p) && Request.tpingPlayers.contains(requestManager.getIncomingRequest(p).getFrom().getUniqueId()))) {
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.no_incoming_requests")).send(p);
            // No incoming requests.
            return true;
        }

        requestManager.getIncomingRequest(p).deny();
        requestManager.deleteRequest(requestManager.getIncomingRequest(p));
        // Denied.
        return true;
    }
}
