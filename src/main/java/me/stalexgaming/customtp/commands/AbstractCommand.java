package me.stalexgaming.customtp.commands;

import me.stalexgaming.customtp.Main;
import me.stalexgaming.customtp.objects.MessageBuilder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alex on 5-7-2016.
 *
 */
 public abstract class AbstractCommand implements CommandExecutor {

    private static List<CommandSender> commandSuccess = new ArrayList<>();
    private static Map<String, List<AbstractCommand>> commands = new HashMap<>();

    private String name;
    private int minArgs;
    private int maxArgs;
    private boolean console;
    private String usage;

    public AbstractCommand(String name, int minArgs, int maxArgs, boolean console, String usage) {
        this.name = name;
        this.minArgs = minArgs;
        this.maxArgs = maxArgs;
        this.console = console;
        this.usage = usage;

        if(!commands.containsKey(name)) {
            Main.getInstance().getCommand(name).setExecutor(this);
        }
        List<AbstractCommand> executors = commands.getOrDefault(name, new ArrayList<>());
        executors.add(this);
        commands.put(name, executors);
    }

    public abstract boolean execute(CommandSender sender, String[] args);

    public String getName() {
        return name;
    }

    public int getMinArgs() {
        return minArgs;
    }

    public int getMaxArgs() {
        return maxArgs;
    }

    public boolean isConsole() {
        return console;
    }

    public String getUsage() {
        return usage;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (commands.containsKey(cmd.getName())) {
            boolean success = false;
            for (AbstractCommand command : commands.get(cmd.getName())) {
                if (command.preExecute(sender, cmd, commandLabel, args)) {
                    success = true;
                }
            }
            return success;
        }
        return false;
    }

    private boolean preExecute(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (commandSuccess.contains(sender)) {
            return true;
        }
        if (!console && sender instanceof ConsoleCommandSender) {
            return true;
        }
        if (!(args.length >= minArgs && args.length <= maxArgs)) {
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.incorrect_usage")).replace("%usage%", usage).send((Player) sender);
            return true;
        }
        boolean result = execute(sender, args);
        if (result) {
            commandSuccess.add(sender);
            new BukkitRunnable() {
                @Override
                public void run() {
                    commandSuccess.remove(sender);
                }
            }.runTaskLater(Main.getInstance(), 4);
        }
        return result;
    }

}
