package me.stalexgaming.customtp.commands;

import me.stalexgaming.customtp.Main;
import me.stalexgaming.customtp.managers.RequestManager;
import me.stalexgaming.customtp.objects.MessageBuilder;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by alex on 5-7-2016.
 */
public class CommandTpAccept extends AbstractCommand {

    private RequestManager requestManager;

    public CommandTpAccept(String name, int minArgs, int maxArgs, boolean console, String usage, RequestManager requestManager) {
        super(name, minArgs, maxArgs, console, usage);
        this.requestManager = requestManager;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        Player p = (Player) sender;
        if(!requestManager.hasIncomingRequest(p)) {
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.no_incoming_requests")).send(p);
            // No incoming requests
            return true;
        }
        requestManager.getIncomingRequest(p).accept();
        requestManager.deleteRequest(requestManager.getIncomingRequest(p));
        // Accepted.
        return true;
    }

}
