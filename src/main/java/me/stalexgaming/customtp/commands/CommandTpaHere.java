package me.stalexgaming.customtp.commands;

import me.stalexgaming.customtp.Main;
import me.stalexgaming.customtp.enums.TeleportationType;
import me.stalexgaming.customtp.managers.RequestManager;
import me.stalexgaming.customtp.objects.MessageBuilder;
import me.stalexgaming.customtp.objects.Request;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by alex on 5-7-2016.
 */
public class CommandTpaHere extends AbstractCommand {

    private RequestManager requestManager;

    public CommandTpaHere(String name, int minArgs, int maxArgs, boolean console, String usage, RequestManager requestManager) {
        super(name, minArgs, maxArgs, console, usage);
        this.requestManager = requestManager;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        Player p = (Player) sender;
        for(Player player : Bukkit.getOnlinePlayers())
            if(player.getDisplayName().startsWith(args[0].replaceFirst("*", ""))) {
                if(requestManager.getIncomingRequest(player) != null)
                    if(requestManager.getIncomingRequest(player).getFrom().equals(p)) {
                        new MessageBuilder(Main.getInstance().getConfig().getStringList("message.request_already_pending")).replace("%player%", player.getName()).replace("%nickname%", player.getName()).replace("%nickname%", player.getDisplayName()).send(p);
                        return true;
                    }
                    
                if(p.equals(player)) {
                    new MessageBuilder(Main.getInstance().getConfig().getStringList("message.cannot_request_yourself")).send(p);
                    return true;
                }
                
                Request request = new Request(p, player, System.currentTimeMillis(), TeleportationType.TPAHERE);
                requestManager.sendRequest(request);
                new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpahere_sent")).replace("%player%", player.getName()).replace("%nickname%", player.getName()).replace("%nickname%", player.getDisplayName()).send(p);
                new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpahere_received")).replace("%player%", p.getName()).replace("%nickname%", player.getName()).replace("%nickname%", player.getDisplayName()).setSpecial(request).send(player);
                return true;
            }
        
        if(Bukkit.getPlayer(args[0]) != null) {
            Player t = Bukkit.getPlayer(args[0].replaceFirst("*", ""));
            if(requestManager.getIncomingRequest(t) != null)
                if(requestManager.getIncomingRequest(t).getFrom().equals(p)) {
                    new MessageBuilder(Main.getInstance().getConfig().getStringList("message.request_already_pending")).replace("%player%", t.getName()).replace("%nickname%", player.getDisplayName()).send(p);
                    return true;
                }
            
            if(p.equals(t)) {
                new MessageBuilder(Main.getInstance().getConfig().getStringList("message.cannot_request_yourself")).send(p);
                return true;
            }
            
            Request request = new Request(p, t, System.currentTimeMillis(), TeleportationType.TPAHERE);
            requestManager.sendRequest(request);
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpahere_sent")).replace("%player%", t.getName()).replace("%nickname%", player.getDisplayName()).send(p);
            new MessageBuilder(Main.getInstance().getConfig().getStringList("message.tpahere_received")).replace("%player%", p.getName()).replace("%nickname%", player.getDisplayName()).setSpecial(request).send(t);
            return true;
        }

        new MessageBuilder(Main.getInstance().getConfig().getStringList("message.invalid_player")).replace("%args%", args[0]).send(p);
        return true;
    }

}
