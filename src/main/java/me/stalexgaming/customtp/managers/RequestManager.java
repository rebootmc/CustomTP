package me.stalexgaming.customtp.managers;

import me.stalexgaming.customtp.Main;
import me.stalexgaming.customtp.objects.Request;
import me.stalexgaming.customtp.objects.MessageBuilder;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.ListIterator;
import java.lang.Runnable;

/**
 * Created by alex on 5-7-2016.
 */
public class RequestManager {

    Main plugin;

    private List<Request> requestList;

    public RequestManager(Main main) {
        this.plugin = main;
        this.requestList = new ArrayList<>();
    }

    public void removeRequests(Player p) {
        List<Request> requestCollection = new ArrayList<>();
        requestList.forEach(r -> requestCollection.add(r));
        requestCollection.forEach(r -> {
            if(r.getTo().equals(p) || r.getFrom().equals(p)) {
                requestList.remove(r);
            }
        });
    }

    public void sendRequest(final Request request) {
        this.requestList.add(request);
        plugin.getServer().getScheduler().runTaskLater(plugin, (Runnable) () -> {
            if(requestList.contains(request)) {
                new MessageBuilder(plugin.getConfig().getStringList("message.request_timeout")).replace("%player%", request.getFrom().getDisplayName()).send(request.getFrom());
                requestList.remove(request);
            }
        }, (plugin.getConfig().getInt("tpa_expire_time")*20L));
    }

    public void deleteRequest(Request request) {
        requestList.remove(request);
    }

    public boolean hasIncomingRequest(Player p) {
        return requestList.stream().filter(r -> r.getTo() == p).collect(Collectors.toList()).size() > 0;
    }

    public boolean hasOutgoingRequests(Player p) {
        return requestList.stream().filter(r -> r.getFrom() == p).collect(Collectors.toList()).size() > 0;
    }

    public Request getIncomingRequest(Player p) {
        ListIterator reverseList = requestList.listIterator(requestList.size());
        while(reverseList.hasPrevious()) {
            Request request = (Request) reverseList.previous();
            if(request.getTo().equals(p)) {
                return request;
            }
        }
        return null;
    }

    public List<Request> getOutgoingRequests(Player p) {
        List<Request> requests = new ArrayList<>();
        for(Request request : requestList) {
            if(request.getTo().equals(p)) {
                requests.add(request);
            }
        }
        return requests;
    }

}
