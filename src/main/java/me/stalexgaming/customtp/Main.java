package me.stalexgaming.customtp;

import me.stalexgaming.customtp.commands.*;
import me.stalexgaming.customtp.listeners.PlayerMove;
import me.stalexgaming.customtp.listeners.PlayerQuit;
import me.stalexgaming.customtp.managers.RequestManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by alex on 5-7-2016.
 */
public class Main extends JavaPlugin {

    private static Main instance;
    private static RequestManager requestManager;

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        requestManager = new RequestManager(this);

        init();
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    public static Main getInstance() {
        return instance;
    }

    public static RequestManager getRequestManager() {
        return requestManager;
    }

    private void init() {
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new PlayerQuit(requestManager), this);
        pm.registerEvents(new PlayerMove(), this);

        new CommandTp("tp", 1, 1, false, "/tp <player>");
        new CommandTpa("tpa", 1, 1, false, "/tpa <player>", requestManager);
        new CommandTpAccept("tpaccept", 0, 99, false, "/tpaccept", requestManager);
        new CommandTpaHere("tpahere", 1, 1, false, "/tpahere <player>",requestManager);
        new CommandTpDeny("tpdeny", 0, 99, false, "/tpdeny",requestManager);
        new CommandTpHere("tphere", 1, 1, false, "/tphere <player>");
    }

}
